<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'FrontController@index')->name('front');

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware'=>'role:ROLE_ADMIN', 'prefix' => 'admin'], function () {
    Route::get('', 'Admin\IndexController@index')->name('adminindex');
    Route::get('demo', 'Admin\IndexController@demo')->name('admindemo');
    Route::get('user/list', 'Admin\UserController@list')->name('adminuser');
    Route::get('user/list', 'Admin\UserController@list')->name('adminuser');
    Route::resource('user', 'Admin\UserController');
    Route::get('group/list', 'Admin\GroupController@list')->name('admingroup');
    Route::resource('group', 'Admin\GroupController');
    Route::get('role/list', 'Admin\RoleController@list')->name('adminrole');
    Route::resource('role', 'Admin\RoleController');
});
Route::group(['middleware'=>'role:ROLE_USER|ROLE_ADMIN', 'prefix' => 'admin'], function () {
    Route::get('customer/list', 'Admin\CustomerController@list')->name('admincustomer');
    Route::resource('customer', 'Admin\CustomerController');

});



Route::get('/chat', 'ChatsController@index');
Route::get('/chat/messages', 'ChatsController@fetchMessages');
Route::post('/chat/messages', 'ChatsController@sendMessage');