<?php

namespace App\Services;

use App\Address;
use App\Customer;
use App\Traits\Service;
use Illuminate\Support\Facades\Log;

class AddressService
{
    use Service;

    /**
     * @param array $data
     * @return Address
     */
    static public function create(array $data)
    {
        $c = new Address();
        if($c->validate($data)) {
            $c->fill($data);
        } else {
            throw new \Exception("Address did not validate");
        }
        $c->save();
        return $c;
    }

    /**
     * @param Address $address
     * @param Address|array $data
     * @return mixed
     */
    static public function validateAndFill(Address $address, $data)
    {
        if(!$address->validate((array)$data)) {
            throw new \Exception("Address did not validate");
        }
        $address->fill((array)$data);
    }

    /**
     * @param Address $customer
     */
    static public function delete(Address $x)
    {
        $x->delete();
    }

    /**
     * @param $id
     * @return Address
     */
    static public function getById($id)
    {
        $c = Address::find($id);
        return $c;
    }

    /**
     * @param Address[]\Address $a
     * @param Customer $c
     */
    static public function addToCustomer($a, Customer $c)
    {
        if(is_array($a))
        {
            self::addArrayToCustomer($a, $c);
        } else {
            self::addSingleToCustomer($a, $c);
        }
    }

    /**
     * @param Address $a
     * @param Customer $c
     */
    static protected function addSingleToCustomer(Address $a, Customer $c)
    {
        $a->customer_id = $c->id;
        $a->save();
    }

    /**
     * @param Address[] $a
     * @param Customer $c
     */
    static protected function addArrayToCustomer(array $a, Customer $c)
    {
        /** @var Address $address */
        foreach($a as $address) {
            $address->customer_id = $c->id;
            $address->save();
        }
    }

    /**
     * @param Address[]|Address $a
     * @param Customer $c
     */
    static public function setCustomerAddresses($a, Customer $c)
    {
        if(!is_array($a))
        {
            $a = [$a];
        }
        // get existing address IDs
        $existingIds = $c->addressIds();
        $usedIds = [];
        foreach($a as $_add)
        {
            $add = self::convertToArray($_add);
            if(array_key_exists("deleted",$add) && $add['deleted']) { continue; }
            if(array_key_exists("trashed",$add) && $add['trashed']) { continue; }
            if(array_key_exists('id', $add))
            {
                Log::info("looking for address id ".$add['id']);
                $address = Address::find($add['id']);
                if(!$address || $address->customer_id!=$c->id)
                {
                    $address = new Address();
                } else {
                    $usedIds[] = $add['id'];
                }
            } else {
                $address = new Address();
            }
            Log::info($add);
            self::validateAndFill($address, $add);
            $address->customer_id = $c->id;
            $address->save();
        }
        // get list of address ids no longer with us...
        $diff = array_diff($existingIds, $usedIds);
        Log::info("address ids no longer used ".print_r($diff,1));
        foreach($diff as $id)
        {
            $addressToDelete = self::getById($id);
            $addressToDelete->delete();
        }
    }


}