<?php

namespace App\Http\Controllers\Admin;

use App\Address;
use App\Customer;
use App\Services\AddressService;
use App\Services\CustomerService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class CustomerController extends Controller
{

    public function list()
    {
        return view("admin.customer.list");
    }

    public function index()
    {
//        $c = Customer::all();
        $c = CustomerService::getAllWithPrimaryAddress();
        $data = ['data'=>$c];
        return Response::json($data);
    }

    public function formshow($id)
    {
        $u = Customer::find($id);
        $data = ['data'=>$u];
        return Response::json($data);
    }

    public function show($id)
    {
        $u = Customer::find($id);
        $u->load("addresses");
        $t = [Address::ADDRESS_TYPE_PHYSICAL, Address::ADDRESS_TYPE_MAILING, Address::ADDRESS_TYPE_OTHER];
        $data = ['data'=>['customer'=>$u,'addressTypes'=>$t]];
        return Response::json($data);
    }

    public function update(Request $request, Response $response, $id)
    {
        $incoming = $request->all();
        $g = CustomerService::getById($id);
        $g->fill($request->all());
        if(!$g->validate($request->all()))
        {
            return new JsonResponse(['errors'=>$g->errors()], 422);
        }
        $g->save();
        $a = $incoming['addresses'];
        AddressService::setCustomerAddresses($a, $g);
        $data = ['customer'=>$g];
        return Response::json($data);
    }

    public function store(Request $request, Response $response)
    {
        $g = new Customer();
        $g->fill($request->all());
        if(!$g->validate($request->all()))
        {
            return new JsonResponse(['errors'=>$g->errors()], 422);
        }
        $c = CustomerService::create($request->all());
        $data = ['role'=>$g];
        return Response::json($data);
    }
}
