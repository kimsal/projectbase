<?php

namespace App\Http\Controllers\Admin;

use App\Group;
use App\Role;
use App\Services\UserService;
use App\Team;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class UserController extends Controller
{
    public function list()
    {
        $u = User::all();
        return view('admin.user.list');
    }

    public function index()
    {
        $u = User::all();
        $data = ['data'=>$u];
        return Response::json($data);
    }

    public function formshow($id)
    {
        $u = UserService::getById($id);
        $data = ['data'=>$u];
        return Response::json($data);
    }

    public function show($id)
    {
        $u = UserService::getById($id);
        $u->load("teams",'groups','roles');
        $r = Role::all();
        $g = Group::all();
        $t = Team::all();
        $data = ['data'=>['user'=>$u, 'teams'=>$t, 'groups'=>$g, 'roles'=>$r]];
        return Response::json($data);
    }

    public function update(Request $request, Response $response, $id)
    {
        $user = UserService::getById($id);
        try {
            UserService::validateAndFill($user, $request->all());
        } catch (\Exception $e)
        {
            return new JsonResponse(['errors'=>$user->errors()], 422);
        }

        UserService::setRoles($user, $request->all()['roles']);
        UserService::setGroups($user, $request->all()['groups']);

        $user->save();

        $data = ['user'=>$user];
        return Response::json($data);
    }

    public function store(Request $request, Response $response)
    {
        $user = new User();
        try {
            UserService::setPassword($user, Input::get('password'));
            UserService::validateAndFill($user, $request->all());
            $user->save();
        } catch (\Exception $e)
        {
            dd($e);
            return new JsonResponse(['errors'=>$user->errors()], 422);
        }
        UserService::setRoles($user, ['ROLE_USER']);
        $user->save();

        $data = ['user'=>$user];
        return Response::json($data);
    }

}
