<?php

namespace App;

use App\Traits\Validatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class User extends Authenticatable
{
    use Notifiable, Validatable;

    public $table = "user";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    public function roles()
    {
        return $this->belongsToMany('App\Role', 'role_user');
    }

    public function teams()
    {
        return $this->belongsToMany('App\Team', 'team_user');
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group', 'group_user');
    }


    /** make sure we have an api_token on each user when created */
    static public function boot()
    {
        User::creating(function ($user) {
            if (!$user->api_token) {
                $user->api_token = Uuid::uuid4();
                return true;
            }
            return true;
        });
    }



    public function setRoles($roles)
    {
        $this->roles()->detach();
        foreach($roles as $role)
        {
            $r = Role::where('identifier',$role)->first();
            $this->roles()->attach($r);
        }
    }

    public function setGroups($groups)
    {
        $this->groups()->detach();
        foreach($groups as $group)
        {
            $r = Group::where('identifier',$group)->first();
            $this->groups()->attach($r);
        }
    }

    public function hasRole($roleIdentifier = "ROLE_USER")
    {
        if(strpos($roleIdentifier,"|")>0)
        {
            $roleIdentifier = explode("|", $roleIdentifier);
        }
        $r = $this->roles()->get();
        foreach($r as $role)
        {
            if(is_array($roleIdentifier))
            {
                if (in_array($role->identifier,$roleIdentifier)) {
                    return true;
                }
            } else {
                if ($role->identifier == $roleIdentifier) {
                    return true;
                }
            }
        }
        return false;
    }

    public function messages()
    {
      return $this->hasMany(Message::class);
    }
}
