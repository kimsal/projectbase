<?php

namespace Tests\Unit;

use App\Note;
use App\Services\CustomerService;
use App\Services\NoteService;
use App\Services\UserService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NoteTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCustomerNote1()
    {
        $c = $this->getSampleCustomer1("c1");
        $u = $this->getSampleUser();
        $n = NoteService::addMessageOnCustomer("sample note 1", $c);
        $this->assertTrue(is_object($n));
        $this->assertTrue($n->note=="sample note 1");
        $this->assertTrue($n->customer_id == $c->id);
    }

    public function testCustomerNote2()
    {
        $c = $this->getSampleCustomer1("c1");
        $u = $this->getSampleUser();
        $n = NoteService::addMessageByUserOnCustomer("sample note 2", $u, $c);
        $this->assertTrue(is_object($n));
        $this->assertTrue($n->note=="sample note 2");
        $this->assertTrue($n->customer_id == $c->id);
        $this->assertTrue($n->author_id == $u->id);
    }

    public function testCustomerNote3()
    {
        $c = $this->getSampleCustomer1("c2");
        $u = $this->getSampleUser();
        $n = NoteService::addMessageByUserOnCustomer("sample note 1", $u, $c);
        $n = NoteService::addMessageByUserOnCustomer("sample note 2", $u, $c);
        $n = NoteService::addMessageByUserOnCustomer("sample note 3", $u, $c);
        $notes = CustomerService::getNotes($c);
        $this->assertTrue(count($notes)==3);
        $this->assertTrue(is_object($n));
        $this->assertTrue($notes[0]->note=="sample note 1");
    }

    public function testCustomerNote4()
    {
        $c = $this->getSampleCustomer1("c2");
        $u = $this->getSampleUser();
        $n = NoteService::addMessageByUserOnCustomer("sample note 1", $u, $c);
        $n = NoteService::addMessageOnCustomer("sample system note 2", $c);
        $n = NoteService::addMessageByUserOnCustomer("sample note 3", $u, $c);
        $notes = CustomerService::getNotesByType($c, Note::TYPE_USER);
        $this->assertTrue(count($notes)==2);
        $notes = CustomerService::getNotesByType($c, Note::TYPE_SYSTEM);
        $this->assertTrue(count($notes)==1);
    }

    public function testCustomerNote5Private()
    {
        $c = $this->getSampleCustomer1("c2");
        $u = $this->getSampleUser();
        $n = NoteService::addMessageByUserOnCustomer("sample note 1", $u, $c);
        $n = NoteService::addMessageOnCustomer("sample system note 2", $c, true);
        $n = NoteService::addMessageByUserOnCustomer("sample note 3", $u, $c);
        $notes = CustomerService::getNotesByType($c, Note::TYPE_USER);
        $this->assertTrue(count($notes)==2);
        $notes = CustomerService::getNotesByType($c, Note::TYPE_USER,  Note::VISIBILITY_PUBLIC);
        $this->assertTrue(count($notes)==2);
        $notes = CustomerService::getNotesByType($c, Note::TYPE_USER,  Note::VISIBILITY_PRIVATE);
        $this->assertTrue(count($notes)==0);
        $notes = CustomerService::getNotesByType($c, Note::TYPE_SYSTEM);
        $this->assertTrue(count($notes)==1);
        $notes = CustomerService::getNotesByType($c, Note::TYPE_SYSTEM, Note::VISIBILITY_PUBLIC);
        $this->assertTrue(count($notes)==0);
        $notes = CustomerService::getNotesByType($c, Note::TYPE_SYSTEM, Note::VISIBILITY_PRIVATE);
        $this->assertTrue(count($notes)==1);

    }


    public function getSampleUser($email="k@k.com")
    {
        $d = ['email'=>$email, 'name'=>$email, 'password'=>$email];
        $u = UserService::create($d);
        return $u;
    }

    /**
     * @return \App\Customer
     */
    public function getSampleCustomer1($first="test1")
    {
        $a = [];
        $a['first_name'] = $first;
        $a['last_name'] = "last1";

        $c = CustomerService::create($a);
        return $c;
    }

}
