<?php

namespace Tests\Unit;

use App\Services\AddressService;
use App\Services\CustomerService;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CustomerTest extends TestCase
{
    use DatabaseTransactions;


    public function testCreatingCustomer()
    {
        $c = $this->getSampleCustomer1();
        $this->assertTrue(is_object($c));
        $this->assertTrue($c->first_name=="test1");
        $c2 =  CustomerService::getById($c->id);
        $this->assertTrue($c2!=null);
        $this->assertTrue($c2->id==$c->id);
        $this->assertTrue($c2->first_name=="test1");
    }

    public function testCustomerWithAddress()
    {
        $c = $this->getSampleCustomer1();
        $a = $this->getSampleAddress1();
        AddressService::addToCustomer($a, $c);
        $c2 =  CustomerService::getById($c->id);
        $addresses = CustomerService::getAddresses($c2);

        $this->assertTrue(count($addresses)==1);
    }

    public function testCustomerWithAddresses()
    {
        $c = $this->getSampleCustomer1();
        $a = $this->getSampleAddress1("sample1",5);
        $a2 = $this->getSampleAddress1("sample2",2);
        AddressService::addToCustomer($a, $c);
        AddressService::addToCustomer($a2, $c);

        $c2 =  CustomerService::getById($c->id);
        Log::info("find addresses for customer id ".$c2->id);

        $addresses = CustomerService::getAddresses($c2);
        foreach($addresses as $add)
        {
            Log::info($add->id.":".$add->customer_id);
        }
        $this->assertTrue(count($addresses)==2);
        $first = $addresses->first();
        Log::info($first);
        $this->assertTrue($first->address1=="sample2");
        $this->assertTrue($first->sort_order==2);

    }

    public function testCustomerWithAddresses2()
     {
         $c = $this->getSampleCustomer1();
         $a = $this->getSampleAddress1("sample1",5);
         $a2 = $this->getSampleAddress1("sample2",2, true);
         AddressService::addToCustomer($a, $c);
         AddressService::addToCustomer($a2, $c);

         $c2 =  CustomerService::getById($c->id);
         Log::info("find addresses for customer id ".$c2->id);
         $all = CustomerService::getAllWithPrimaryAddress();
         $this->assertTrue(count($all)==1);
         $this->assertTrue($all[0]->address1=="sample2");
         $this->assertTrue($all[0]->state=="NY");

     }


    public function testCustomerWithAddressReset()
    {
        $c = $this->getSampleCustomer1();
        $a = $this->getSampleAddress1("sample1",5);
        $a2 = $this->getSampleAddress1("sample2",2);
        AddressService::addToCustomer($a, $c);
        AddressService::addToCustomer($a2, $c);

        $c2 =  CustomerService::getById($c->id);
        Log::info("find addresses for customer id ".$c2->id);

        $addresses = CustomerService::getAddresses($c2);
        foreach($addresses as $add)
        {
            Log::info($add->id.":".$add->customer_id);
        }
        $this->assertTrue(count($addresses)==2);


        $aNew1 = $this->getSampleAddress1("new1",17);
        AddressService::setCustomerAddresses($aNew1, $c2);
        $addresses = CustomerService::getAddresses($c2);
        $this->assertTrue(count($addresses)==1);
        $a1 = $addresses->first();
        $this->assertTrue($a1->address1=="new1");
        $this->assertTrue($a1->sort_order==17);

    }


    /**
     * @return \App\Customer
     */
    public function getSampleCustomer1($first="test1")
    {
        $a = [];
        $a['first_name'] = $first;
        $a['last_name'] = "last1";

        $c = CustomerService::create($a);
        return $c;
    }

    /**
     * @return \App\Address
     */
    public function getSampleAddress1($address1="123 sesame", $order = 1, $primary = false)
    {
        $a = [];
        $a['address1'] = $address1;
        $a['address2'] = "n/a";
        $a['city'] = "New York";
        $a['state'] = "NY";
        $a['postal_code'] = "12345";
        $a['note'] = "note";
        $a['is_primary'] = $primary;
        $a['sort_order'] = $order;

        $c = AddressService::create($a);
        return $c;
    }
}