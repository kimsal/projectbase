
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

Vue = require('vue');
var VueResource = require('vue-resource');
Vue.use(VueResource);


import adminusertable from './components/AdminUserTable.vue';
import adminuseredit from './components/AdminUserEdit.vue';
import adminusernew from './components/AdminUserNew.vue';
import VueRouter from 'vue-router';

const routes = [
  { path: '/', component: adminusertable,name:"adminusertable" },
  { path: '/edit/:id', component: adminuseredit, name:"adminuseredit" },
  { path: '/new', component: adminusernew, name:"adminusernew" },
];
const router = new VueRouter({
  routes:routes // short for routes: routes
});


window.ua = new Vue({
    router:router,
    el: '#useradmintable',
    components: {
        adminusertable: adminusertable
    },
    data: {
        table_columns: [
            {label: 'First Name', field: 'first_name'},
            {label: 'Last Name', field: 'last_name'},
            {label: 'Email', field: 'email'},
        ],
        table_rows: []
    },
    mounted() {}
});

$(function(){
    $("[data-hide]").on("click", function(){
        $("." + $(this).attr("data-hide")).hide();
    });
});
