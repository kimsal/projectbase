
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

Vue = require('vue');
var VueResource = require('vue-resource');
Vue.use(VueResource);


import admingrouptable from './components/AdminGroupTable.vue';
import admingroupedit from './components/AdminGroupEdit.vue';
import admingroupnew from './components/AdminGroupNew.vue';
import VueRouter from 'vue-router';

const routes = [
  { path: '/', component: admingrouptable,name:"admingrouptable" },
  { path: '/edit/:id', component: admingroupedit, name:"admingroupedit" },
  { path: '/new', component: admingroupnew, name:"admingroupnew" },
];
const router = new VueRouter({
  routes:routes // short for routes: routes
});


window.ua = new Vue({
    router:router,
    el: '#admingrouptable',
    components: {
        admingrouptable: admingrouptable
    },
    data: {
        table_columns: [
            {label: 'Name', field: 'name'},
            {label: 'Identifier', field: 'identifier'},
        ],
        table_rows: []
    },
    mounted() {}
});

$(function(){
    $("[data-hide]").on("click", function(){
        $("." + $(this).attr("data-hide")).hide();
    });
});
