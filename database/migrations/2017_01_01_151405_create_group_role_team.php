<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupRoleTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // groups are admin-defined groups
        // teams are user-defined groups
        // role is label for functional role (admin, user, editor, etc)
        //
        Schema::create('group', function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('name');
                    $table->string('identifier');
                    $table->timestamps();
                });
        Schema::create('role', function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('name');
                    $table->string('identifier');
                    $table->timestamps();
                });
        Schema::create('team', function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('name');
                    $table->string('identifier');
                    $table->timestamps();
                });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team');
        Schema::dropIfExists('group');
        Schema::dropIfExists('role');
    }
}
