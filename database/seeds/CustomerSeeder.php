<?php

use App\Address;
use App\Customer;
use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // let's have 5 'always the same' customer data sets
        for ($x = 1; $x < 5; $x++) {

            $c             = new Customer();
            $c->first_name = "Customer $x";
            $c->last_name  = "Last name $x";
            $c->save();

            $a              = new Address();
            $a->customer_id = $c->id;
            $a->address1    = "123 sesame street";
            $a->address2    = "n/a";
            $a->city        = "New York";
            $a->state       = "NY";
            $a->is_primary  = true;
            $a->postal_code = "12345";
            $a->sort_order  = 4;
            $a->save();

            $a2              = new Address();
            $a2->customer_id = $c->id;
            $a2->address1    = "111 White street";
            $a2->address2    = "n/a";
            $a2->city        = "Clinton Twp";
            $a2->state       = "MI";
            $a2->postal_code = "48197";
            $a2->sort_order  = 2;
            $a2->save();
            echo "customer $x done\n";
        }

        // let's have 500 random customers
        for ($x = 1; $x <= 500; $x++) {
            $faker         = Faker::create();
            $c             = new Customer();
            $c->first_name = $faker->firstName;
            $c->last_name = $faker->lastName;
            $c->save();

            $a              = new Address();
            $a->customer_id = $c->id;
            $a->address1    = $faker->streetAddress;
            $a->address2    = $faker->secondaryAddress;
            $a->city        = $faker->city;
            $a->state       = $faker->stateAbbr;
            $a->is_primary  = true;
            $a->postal_code = $faker->postcode;
            $a->sort_order  = 4;
            $a->save();

            $faker         = Faker::create();
            $a2              = new Address();
            $a2->customer_id = $c->id;
            $a2->address1    = $faker->streetAddress;
            $a2->address2    = $faker->secondaryAddress;
            $a2->city        = $faker->city;
            $a2->state       = $faker->stateAbbr;
            $a2->is_primary  = false;
            $a2->postal_code = $faker->postcode;
            $a2->sort_order  = 2;
            $a2->save();
            echo "customer $x done\n";

        }

    }
}
